"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var $__request__,
    $__boom__;
var request = ($__request__ = require("request"), $__request__ && $__request__.__esModule && $__request__ || {default: $__request__}).default;
var Boom = ($__boom__ = require("boom"), $__boom__ && $__boom__.__esModule && $__boom__ || {default: $__boom__}).default;
function auth_check(url) {
  function checkSession(authSession) {
    if (Array.isArray(authSession))
      authSession = authSession[0];
    return new Promise((function(resolve, reject) {
      request.get(url, {
        headers: {Cookie: 'AuthSession=' + authSession},
        strictSSL: false
      }, function(err, res, body) {
        if (!err && res.statusCode === 200) {
          return resolve(JSON.parse(body));
        }
        reject(err || Boom.unauthorized('invalid session'));
      });
    }));
  }
  return {checkSession: checkSession};
}
var $__default = auth_check;
