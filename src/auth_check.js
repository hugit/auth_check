/**
 * Created by acomitevski on 18/12/14.
 */

import request from 'request';
import Boom from 'boom';

function auth_check(url) {

  function checkSession(authSession) {
    // TODO: DEV-780 fix duplicated AuthSession cookies
    if (Array.isArray(authSession))
      authSession = authSession[0];
    return new Promise((resolve, reject) => {
      request.get(url, {
        headers: {
          Cookie: 'AuthSession=' + authSession
        },
        strictSSL: false
      }, function(err, res, body) {
        if (!err && res.statusCode === 200) {
          return resolve(JSON.parse(body));
        }
        reject(err || Boom.unauthorized('invalid session'));
      });
    });
  }

  return {
    checkSession
  };
}

export default auth_check;
