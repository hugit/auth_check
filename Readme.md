# Authentication Check

## Usage

```javascript

    import auth_check from 'auth_check';
    let authCheck = auth_check(`${config.apiHttps}/authentication/check`);
    
    authCheck.checkSession(authSession)
      .then(function (user) {
        console.log(user);
      }, function (err) {
        console.log(err);
      });
    
    
```


## Pushing Changes

The library is written in EcmaScript 6.
Before pushing a change make sure to run

```npm run-script prepublish```

so the EcmaScript 5 version gets built