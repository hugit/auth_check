/**
 * Created by acomitevski on 12/11/14.
 */

require('traceur/bin/traceur-runtime');
var auth_check = require('./lib/auth_check');
// traceur exports in .default key
module.exports = auth_check.default;
